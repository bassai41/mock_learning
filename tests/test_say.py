from main.say import Foo, Hoge

def test_foo_say():
    assert Foo().say() == "foo"

def test_foo_say2():
    assert Foo().say2() == "foo2"