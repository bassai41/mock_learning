from main.calc import Calc

def test_add_01():
    assert Calc(9,2).add() == 11

def test_add_02():
    assert Calc(-9,2).add() == -7

def test_add_03(mocker):
    mocker.patch("main.calc.Calc.return3", return_value=4)
    print(Calc(1,2).kaedama())
    assert Calc(1,2).kaedama() == 5
