class Calc: 
    """
    hogehoge
    """

    def __init__(self, a, b):
        """Constructor of Calc.

        This function sets up this object.

        Parameters
        ---------
        a : int
            A value of first number
        b : int
            A value of second number

        Examples
        --------
        >>> Calc(2,3)

        """
        self.a = a
        self.b = b

    def add(self):
        """ calculate a+b (a and b is given at constructor)

        Parameters
        ---------
        a : int
            A value of first number
        b : int
            A value of second number

        Examples
        --------
        >>> Calc(2,3).add()
        5
        """
        return self.a + self.b

    def dif(self):
        """ calculate a+b (a and b is given at constructor)

        Parameters
        ---------
        a : int
            A value of first number
        b : int
            A value of second number

        Examples
        --------
        >>> Calc(2,3).dif()
        -1
        """
        return self.a - self.b

    def seki(self):
        """ calculate a+b (a and b is given at constructor)

        Parameters
        ---------
        a : int
            A value of first number
        b : int
            A value of second number

        Examples
        --------
        >>> Calc(2,3).seki()
        6
        """
        return self.a*self.b

    def shou(self):
        """ calculate a+b (a and b is given at constructor)

        Parameters
        ---------
        a : int
            A value of first number
        b : int
            A value of second number

        Examples
        --------
        >>> Calc(4,2).shou()
        2
        """
        return self.a/self.b

    def return3(self):
        """ calculate a+b (a and b is given at constructor)

        Parameters
        ---------
        a : int
            A value of first number
        b : int
            A value of second number

        Examples
        --------
        >>> Calc(2,3).return3()
        3
        """
        return 3

    def kaedama(self):
        """ calculate a+b (a and b is given at constructor)

        Parameters
        ---------
        a : int
            A value of first number
        b : int
            A value of second number

        Examples
        --------
        >>> Calc(2,3).kaedama()
        5
        """
        c = self.return3()
        return self.a + c