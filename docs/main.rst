main package
============

Submodules
----------

main.calc module
----------------

.. automodule:: main.calc
   :members:
   :undoc-members:
   :show-inheritance:

main.say module
---------------

.. automodule:: main.say
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: main
   :members:
   :undoc-members:
   :show-inheritance:
