tests package
=============

Submodules
----------

tests.test\_calc module
-----------------------

.. automodule:: tests.test_calc
   :members:
   :undoc-members:
   :show-inheritance:

tests.test\_say module
----------------------

.. automodule:: tests.test_say
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tests
   :members:
   :undoc-members:
   :show-inheritance:
